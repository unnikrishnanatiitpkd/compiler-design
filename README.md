# README #

This Repo Contains materials for CS 3300 Compiler Design Course.

### Syllabus ###
Introduction to language translators and overview of the compilation process: Lexical analysis:
specification of tokens, token recognition, conflict resolution.; Parsing: Overview of CFG, Parse trees and derivations.

left recursion, left factoring, top-down parsing, LALR parsing, conflict resolution, dangling-
else; 

Syntax directed translation. Semantic analysis, Type checking, intermediate code generation;

Runtime environments: activation records, heap management; Code optimization: basic blocks, liveness,
register allocation; Advanced topics: Overview of machine dependent and independent optimizations.


### Books  ###
Prescibed Book

Modern Compiler Implementation in ML. Andrew W. Appel

Reference Book

Compilers: Principles, Techniques, and Tools by Alfred V. Aho, Monica S. Lam, Ravi Sethi, and Jeffrey D. Ullman. (Dragon Book)

